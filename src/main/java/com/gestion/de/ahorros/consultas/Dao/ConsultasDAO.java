package com.gestion.de.ahorros.consultas.Dao;


import com.gestion.de.ahorros.consultas.Entitys.Consulta;
import com.gestion.de.ahorros.consultas.Entitys.Consultar;

import org.springframework.stereotype.Repository;

@Repository
public class ConsultasDAO {
    private static Consultar list = new Consultar();
    
    static 
    {
        list.getconsultList().add(new Consulta(1, "152522", 2005.25, "Corriente"));
        list.getconsultList().add(new Consulta(2, "585222", 1003.52, "Credito"));
        list.getconsultList().add(new Consulta(3, "75552", 1023.52, "Ahorros"));
    }
    
    public Consultar getAllconsult() 
    {
        return list;
    }
    public void addconsult(Consulta consult) {
        list.getconsultList().add(consult);
    }
 
}
