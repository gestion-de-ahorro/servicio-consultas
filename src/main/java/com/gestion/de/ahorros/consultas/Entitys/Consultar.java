package com.gestion.de.ahorros.consultas.Entitys;


import java.util.ArrayList;
import java.util.List;

public class Consultar 
{
    private List<Consulta> consultList;
    
    public List<Consulta> getconsultList() {
        if(consultList == null) {
            consultList = new ArrayList<>();
        }
        return consultList;
    }
 
    public void setconsultList(List<Consulta> consultList) {
        this.consultList = consultList;
    }
}


