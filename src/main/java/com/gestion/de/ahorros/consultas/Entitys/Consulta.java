package com.gestion.de.ahorros.consultas.Entitys;

public class Consulta {
    private Integer id;
    private String cuenta;
    private Double saldo;
    private String tipo;


    public Consulta(){

    }
    public Consulta(Integer id,String cuenta,Double saldo,String tipo){
        super();
        this.id=id;
        this.cuenta=cuenta;
        this.saldo=saldo;
        this.tipo=tipo;

        
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getCuenta() {
        return cuenta;
    }
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
    public Double getSaldo() {
        return saldo;
    }
    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
    public String gettipo() {
        return tipo;
    }
    public void settipo(String tipo) {
        this.tipo = tipo;
    }
        
    @Override
    public String toString() {
        return "Consultas [cuenta=" + cuenta + ", id=" + id + ", saldo=" + saldo + ", tipo=" + tipo + "]";
    }
}
