package com.gestion.de.ahorros.consultas.Controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

import com.gestion.de.ahorros.consultas.Dao.ConsultasDAO;
import com.gestion.de.ahorros.consultas.Entitys.Consulta;
import com.gestion.de.ahorros.consultas.Entitys.Consultar;

@RestController
@RequestMapping(path = "/consultaslis")
public class MainController 
{
    @Autowired
    public ConsultasDAO consultDao;
    
    @GetMapping(path="/", produces = "application/json")
    public Consultar getconsult() 
    {
        return consultDao.getAllconsult();
    }
    @PostMapping(path= "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addconsult(@RequestBody Consulta consult) 
    {
        Integer id = consultDao.getAllconsult().getconsultList().size() + 1;
        consult.setId(id);
         
        consultDao.addconsult(consult);
         
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                                    .path("/{id}")
                                    .buildAndExpand(consult.getId())
                                    .toUri();
         
        return ResponseEntity.created(location).build();
    }
     
}
